package com.seeketing.seekdeals;

import com.seeketing.sdks.refs.gcm.GCMBaseKeys;

/**
 * Created by arivas on 27/1/16.
 */
public interface GCMPrivateKeys extends GCMBaseKeys {

    // SENDER_ID. API CONSOLE IDENTIFIER (OVERVIEW API CONSOLE)
    public final String SENDER_ID = "691122524766";
    // SERVER_KEY. CREDENTIAL NEEDED IN ORDER TO USE GCM SERVICE (API CONSOLE)
    public final String SERVER_KEY = "AIzaSyD0AtFF3Lviin8U4LxxPCAfJXDC5Mjfs0M";
}