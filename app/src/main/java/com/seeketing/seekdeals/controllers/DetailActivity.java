package com.seeketing.seekdeals.controllers;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.DealItem;
import com.seeketing.seekdeals.models.DealsDatabaseHelper;

import java.text.NumberFormat;

/**
 * Created by arivas on 28/3/16.
 */
public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        Bundle bundle = getIntent().getExtras();
        Integer id = bundle.getInt("id");

        DealsDatabaseHelper dealsDatabaseHelper = new DealsDatabaseHelper(getApplicationContext());
        final DealItem deal = dealsDatabaseHelper.getDealById(id);

        TextView textName = (TextView) findViewById(R.id.textName);
        textName.setText(deal.getName());
        TextView textDesc = (TextView) findViewById(R.id.textDescription);
        textDesc.setText(deal.getDescription());
        ImageView image = (ImageView) findViewById(R.id.imageView2);
        image.setImageBitmap(deal.getImage());
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView2);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        ((RelativeLayout)linearLayout.getParent()).removeView(linearLayout); //liberamos el layout de su parent
        scrollView.addView(linearLayout);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sets.trackEventClck("DetailImageClick");
                Sets.trackEventEcom("DetailImageClick", deal.getImageUrl().toString(), deal.getId().toString(), deal.getDescription());
                Sets.trackEventMdia("DetailImageClick", deal.getImageUrl().toString(), deal.getId(), "image", deal.getDescription());
            }
        });

        TextView textPrice = (TextView) findViewById(R.id.textAmount);
        textPrice.setText(NumberFormat.getCurrencyInstance().format(deal.getPrice()));

        ViewGroup view = (ViewGroup)getWindow().getDecorView();
        Sets.trackEventNavi("DetailActivity", 50, null);
        Sets.trackEventImpr(view, "DetailActivity", null);

        changeAppParameters();
    }

    private void changeAppParameters() {
        AppIdData dataUser = new AppIdData(getBaseContext());
        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(dataUser.getName());

        int backgroundColor = Color.parseColor(dataUser.getBackgroundColor());
        int textColor = Color.parseColor(dataUser.getTextColor());
        int color = Color.parseColor(dataUser.getTextColor());
        int textColorOther = Color.argb(100, Color.red(color), Color.green(color), Color.blue(color));

        title.setTextColor(textColor);
        title.setBackgroundColor(backgroundColor);

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.setBackgroundColor(backgroundColor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(backgroundColor);
    }
}
