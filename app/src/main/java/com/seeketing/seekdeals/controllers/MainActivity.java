package com.seeketing.seekdeals.controllers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.seeketing.sdks.refs.Refs;
import com.seeketing.sdks.sets.CustomizableClass;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.sdks.sets.UtilComms;
import com.seeketing.seekdeals.GCMPrivateKeys;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(MainActivity.class.getCanonicalName(),"Is null this: " + (Uri.parse("9999")));
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), NotyActivity.class);
                startActivity(intent);
            }
        });
        AppIdData dataUser = new AppIdData(getApplicationContext());
        changeAppParameters(dataUser);
        // START SESSION
        /**
         * These methods are mandatory to start working with REFS
         */
        Refs.startSession(this, true, true, true, dataUser.getAppId(), GCMPrivateKeys.SENDER_ID, UtilComms.REM_PUB_13_PRODUCTION, "http://www.siketing.com");
        /**
         * Look for notifications will
         */
        Refs.setIcons(R.drawable.icon, R.drawable.smallicon);

        Refs.lookForNotifications(this, this.getIntent().getExtras(), null);
        Log.i(MainActivity.class.getCanonicalName(), "Token" + GCMRegistrar.getRegistrationId(this));
    }

    private void changeAppParameters(AppIdData dataUser) {
        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(dataUser.getName());

        int backgroundColor = Color.parseColor(dataUser.getBackgroundColor());
        int textColor = Color.parseColor(dataUser.getTextColor());
        int color = Color.parseColor(dataUser.getTextColor());
        int textColorOther = Color.argb(100, Color.red(color), Color.green(color), Color.blue(color));

        title.setTextColor(textColor);
        title.setBackgroundColor(backgroundColor);

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.setBackgroundColor(backgroundColor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(backgroundColor);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabTextColors(textColorOther, textColor);
        tabLayout.setBackgroundColor(backgroundColor);
        tabLayout.setSelectedTabIndicatorColor(textColor);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundColor(backgroundColor);
        fab.setRippleColor(textColor);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Sets.trackEventNavi("Listado", 10, null);
                    return ListFragment.newInstance(position + 1);
                case 1:
                    Sets.trackEventNavi("Mapa", 20, null);
                    return MapFragment.newInstance(position + 1);
                case 2:
                    Sets.trackEventNavi("Contacto", 30, null);
                    return ContactFragment.newInstance(position + 1);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.list);
                case 1:
                    return getString(R.string.map);
                case 2:
                    return getString(R.string.contact);
            }
            return null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    /**
     * It is important to implement Refs.resumeSession, Refs.stopSession, Refs.endSession in onResume, onStop, onDestroy
     */

    /**
     @name onResume
     @desc We will resume the SDK session state when the app return from background to foreground
     */
    @Override
    protected void onResume() {
        super.onResume();
        Refs.resumeSession(this);
    }

    /**
     @name onStop
     @desc We will stop the SDK session state when the app go to background from foreground
     */
    @Override
    protected void onStop() {
        super.onStop();
        Refs.stopSession(this);
    }

    /**
     @name onDestroy
     @desc We will take advantage of onDestroy method and its lifecycle within
     the application to close refs session and stop communications between *server and app.
     */
    @Override

    protected void onDestroy() {
        super.onDestroy();
        Refs.endSession();
    }
}
