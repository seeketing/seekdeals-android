package com.seeketing.seekdeals.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.CategoriesDatabaseHelper;
import com.seeketing.seekdeals.models.CategoryItem;
import com.seeketing.seekdeals.models.DealItem;
import com.seeketing.seekdeals.models.Timer;
import com.seeketing.seekdeals.views.DealItemsListAdapter;
import com.seeketing.seekdeals.models.DealsDatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by arivas on 3/3/16.
 */
public class ListFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private ListView listView;
    private DealItemsListAdapter adapter;
    private List<List<DealItem>> dealsCategorized;

    public ListFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ListFragment newInstance(int sectionNumber) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        //Enviar navi de entrada a sección
        Sets.trackEventNavi("List", 0, null);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_fragment, container, false);

        listView = (ListView) rootView.findViewById(R.id.listView);

        DealsDatabaseHelper dealsDatabaseHelper = new DealsDatabaseHelper(getContext());
        CategoriesDatabaseHelper categoriesDatabaseHelper = new CategoriesDatabaseHelper(getContext());
        List<CategoryItem> categories = categoriesDatabaseHelper.getAllItems();

        Sets.trackEventImpr(rootView, "listado", null);

        dealsCategorized = dealsDatabaseHelper.getDealsForCategory();

        adapter = new DealItemsListAdapter(getContext(), R.layout.listview_item, dealsCategorized, categories);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject json = adapter.getObjectByPosition(position);

                int section = 0;
                int row = 0;

                try {
                    section = json.getInt("section");
                    row = json.getInt("row");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (row != 0) {
                    row -= 1;

                    DealItem dealItem = dealsCategorized.get(section).get(row);
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("id", dealItem.getId());
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }
}
