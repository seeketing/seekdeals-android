package com.seeketing.seekdeals.controllers;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.Timer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 17/3/16.
 */
public class ContactFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    View rootView;
    TextView textView;
    EditText nombre, apellidos, phone, email;
    EditText[] listFields;
    Button enviar;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ContactFragment newInstance(int sectionNumber) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ContactFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.contact_fragment, container, false);
        }

        textView = (TextView) rootView.findViewById(R.id.textContact);

        nombre = (EditText) rootView.findViewById(R.id.editNombre);
        apellidos = (EditText) rootView.findViewById(R.id.editApellidos);
        phone = (EditText) rootView.findViewById(R.id.editPhone);
        email = (EditText) rootView.findViewById(R.id.editEmail);

        listFields = new EditText[]{nombre, apellidos, phone, email};

        ScrollView scrollView = (ScrollView) rootView.findViewById(R.id.scrollView2);
        LinearLayout linearLayoutInterno = (LinearLayout) rootView.findViewById(R.id.linearLayoutInterno);

        if(linearLayoutInterno.getParent() != scrollView) {
            ((LinearLayout) linearLayoutInterno.getParent()).removeView(linearLayoutInterno); //liberamos el layout de su parent
            scrollView.addView(linearLayoutInterno);
        }

        enviar = (Button) rootView.findViewById(R.id.button);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enviar form
                if (datosValidos()) {
                    String body = nombre.getText() + " " + apellidos.getText() + " solicita contactar con seeketing. \nTelefono: " + phone.getText() + "\nCorreo: " + email.getText();
                    String[] recipients = new String []{"contacto@seeketing.com"};

                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Solicitud de contacto");
                    intent.putExtra(Intent.EXTRA_TEXT, body);

                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.chooseEmail)));

                    Sets.trackEventText("Enviado form de contacto", email.getText().toString(), null);
                } else {
                    Toast.makeText(getContext(), R.string.alertaRelleneDatos, Toast.LENGTH_SHORT).show();
                }
            }
        });

        configureViewColors();

        Sets.trackEventImpr(rootView, "contacto", null);

        return rootView;
    }

    private boolean datosValidos() {
        if(nombre.getText().length() > 2 && apellidos.getText().length() > 2 && phone.getText().length() == 9
            && email.getText().length() > 5)
            return true;

        return false;
    }

    private void configureViewColors() {
        AppIdData dataUser = new AppIdData(getContext());

        int backgroundColor = Color.parseColor(dataUser.getBackgroundColor());
        int textColor = Color.parseColor(dataUser.getTextColor());
        int color = Color.parseColor(dataUser.getTextColor());
        int textColorOther = Color.argb(100, Color.red(color), Color.green(color), Color.blue(color));

        enviar.setBackgroundColor(backgroundColor);
        enviar.setTextColor(textColor);

        textView.setTextColor(textColor);

        for (EditText field : listFields) {
            field.setTextColor(textColor);
            field.setHintTextColor(textColorOther);
        }
    }
}
