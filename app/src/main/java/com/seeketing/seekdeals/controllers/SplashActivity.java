package com.seeketing.seekdeals.controllers;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.seeketing.sdks.refs.Refs;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.GCMPrivateKeys;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.CategoryItem;
import com.seeketing.seekdeals.models.DealItem;
import com.seeketing.seekdeals.models.DealerItem;

public class SplashActivity extends AppCompatActivity implements GCMPrivateKeys {

    private AppIdData dataUser;

    String notifId;
    Bundle bun = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        bun = getIntent().getExtras();

        dataUser = new AppIdData(getApplicationContext());

        if (!dataUser.getConfigured()) {
            showAlert();
        } else {
            loadDealsData();
        }
    }

    EditText textEditor;

    private void showAlert() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);

        Bundle extra = getIntent().getExtras();

        textEditor = (EditText) dialog.findViewById(R.id.textEdit);
        if (extra != null) {
            textEditor.setText(extra.getString("text"));
            textEditor.setSelection(textEditor.getText().length());
        }

        Button dialogButtonVer = (Button) dialog.findViewById(R.id.dialogButtonConfirm);
        dialogButtonVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    Boolean validCode = dataUser.getUserData(textEditor.getText().toString());

                    if (validCode) {
                        dialog.cancel();

                        loadDealsData();
                    } else {
                        Toast.makeText(SplashActivity.this, R.string.alertInvalidCode, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(SplashActivity.this, R.string.sinConexion, Toast.LENGTH_LONG).show();
                }
            }
        });

        Button dialogButtonSubir = (Button) dialog.findViewById(R.id.dialogButtonNoCode);
        dialogButtonSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] recipients = {"Seeketing SL <contact@seeketing.com>"};
                Intent email = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
                // prompts email clients only
                email.putExtra(Intent.EXTRA_EMAIL, recipients);

                try {
                    startActivity(Intent.createChooser(email, getString(R.string.chooseEmail)));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(SplashActivity.this, R.string.alertNoEmailClient, Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    protected void onSaveInstanceState(Bundle extra) {
        super.onSaveInstanceState(extra);

        if (textEditor != null) {
            extra.putString("text", textEditor.getText().toString());
        }
    }

    private void loadDealsData() {
        configureViewColors();

        new DownloadDataTask().execute(new Integer[]{0});
        new DownloadDataTask().execute(new Integer[]{1});
        new DownloadDataTask().execute(new Integer[]{2});


    }

    private void configureViewColors() {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.splashRelative);
        layout.setBackgroundColor(Color.parseColor(dataUser.getBackgroundColor()));

        TextView text = (TextView) findViewById(R.id.nameText);
        text.setText(dataUser.getName());
        text.setTextColor(Color.parseColor(dataUser.getTextColor()));

        ImageView imageView = (ImageView) findViewById(R.id.logoView);
        byte[] decodedBytes = Base64.decode(dataUser.getLogo(), 0);
        imageView.setImageBitmap(BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length));


        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getProgressDrawable().setColorFilter(Color.parseColor(dataUser.getTextColor()), android.graphics.PorterDuff.Mode.SRC_IN);
        progressBar.setProgress(0);

        TextView loadingText = (TextView) findViewById(R.id.textView2);
        loadingText.setText(R.string.infoLoadingCategories);

        loadingText.setTextColor(Color.parseColor(dataUser.getTextColor()));

        ScrollView scView = (ScrollView) findViewById(R.id.scView);
        RelativeLayout linLayout = (RelativeLayout) this.findViewById(R.id.linLayout);
        layout.removeView(linLayout);
        scView.addView(linLayout);

        Configuration config = getResources().getConfiguration();
        if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)imageView.getLayoutParams();
            lp.setMargins(0, 100, 0, 0);
            imageView.setLayoutParams(lp);
            imageView.requestLayout();

            lp = (RelativeLayout.LayoutParams)loadingText.getLayoutParams();
            lp.setMargins(0, 0, 0, 300);
            loadingText.setLayoutParams(lp);
            loadingText.requestLayout();
        }

        text.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        loadingText.setVisibility(View.VISIBLE);
    }

    private class DownloadDataTask extends AsyncTask<Integer, Integer, Integer> {
        protected Integer doInBackground(Integer... caseNumber) {
            switch (caseNumber[0]) {
                case 0:
                    CategoryItem.getCategoriesFromServer(dataUser.getAppId(), getApplicationContext());
                    break;
                case 1:
                    DealerItem.getDealersFromServer(dataUser.getAppId(), getApplicationContext());
                    break;
                case 2:
                    DealItem.getDealsFromServer(dataUser.getAppId(), getApplicationContext());
                    break;
            }

            return caseNumber[0];
        }

        protected void onPostExecute(Integer result) {
            TextView loadingText = (TextView) findViewById(R.id.textView2);
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

            switch (result) {
                case 0:
                    progressBar.setProgress(progressBar.getProgress() + 33);
                    loadingText.setText(R.string.infoLoadingDealers);
                    break;
                case 1:
                    progressBar.setProgress(progressBar.getProgress() + 33);
                    loadingText.setText(R.string.infoLoadingDeals);
                    break;
                case 2:
                    progressBar.setProgress(100);
                    loadingText.setText(R.string.infoLoadingComplete);

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra(GCMPrivateKeys.KEY_ID, notifId);
                    if(bun != null) {
                        intent.putExtras(bun);
                    }
                    startActivity(intent);

                    break;
            }
        }
    }
}
