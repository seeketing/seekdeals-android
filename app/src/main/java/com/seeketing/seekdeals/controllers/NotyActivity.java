package com.seeketing.seekdeals.controllers;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.seeketing.sdks.refs.ElemWindow;
import com.seeketing.sdks.refs.Element;
import com.seeketing.sdks.refs.Refs;
import com.seeketing.sdks.refs.UpdateElementsListener;
import com.seeketing.sdks.refs.interfaces.IElement;
import com.seeketing.sdks.refs.util.UtilDataBase;
import com.seeketing.sdks.refs.util.UtilNetwork;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.Timer;
import com.seeketing.seekdeals.views.NotiAdapter;

import java.util.ArrayList;

/**
 * Created by arivas on 7/3/16.
 */
public class NotyActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, UpdateElementsListener {

    private UtilDataBase dbHandler;

    private NotiAdapter adapter;
    private ArrayList<ElemWindow> promoList = new ArrayList<ElemWindow>();
    ListView list;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHandler = UtilDataBase.getInstance();

        setContentView(R.layout.activity_noty);

        list = (ListView) findViewById(R.id.noti_tabla);
        text = (TextView) findViewById(R.id.emptyTable);

        adapter = new NotiAdapter(getBaseContext(), R.layout.oferta_item, promoList);
        adapter.setPackageName(getBaseContext().getPackageName());
        list.setAdapter(adapter);

        list.setOnItemClickListener(this);
        text.setVisibility(View.GONE);

        Refs.setUpdateElementsListener(this);

        dbHandler.open();

        updatePromoList();

        changeAppParameters();

        ViewGroup view = (ViewGroup)getWindow().getDecorView();
        Sets.trackEventNavi("Noty", 40, null);
        Sets.trackEventImpr(view, "Noty", null);

    }

    private void changeAppParameters() {
        AppIdData dataUser = new AppIdData(getBaseContext());
        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(dataUser.getName());

        int backgroundColor = Color.parseColor(dataUser.getBackgroundColor());
        int textColor = Color.parseColor(dataUser.getTextColor());
        int color = Color.parseColor(dataUser.getTextColor());

        title.setTextColor(textColor);
        title.setBackgroundColor(backgroundColor);
        //getResources().getColor(R.color.colorPrimaryDark)
        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.setBackgroundColor(backgroundColor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(backgroundColor);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(UtilNetwork.isNetworkAvailable(getBaseContext())) {
            RelativeLayout promoContainer = (RelativeLayout) findViewById(R.id.promoContainer);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            //toolbar.setVisibility(View.GONE);
            Refs.showWindow((int) id, promoContainer);
            updatePromoList();
        } else {
            Toast.makeText(getBaseContext(), R.string.sinConexion, Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePromoList() {
        promoList.clear();

        Cursor c = null;
        synchronized (dbHandler) {
            c = dbHandler.getElemByType(UtilDataBase.TABLE_ID_ELEMENTS, IElement.TYPE_WNDN);
            Log.d("NOTY", "Actualizando");
        }

        if (c.moveToFirst()) {
            do {
                Element promoElem = new Element(c);
                if (!promoElem.hasExpired()){
                    promoList.add(promoElem.getWindow());
                }
            } while (c.moveToNext());
        }
        if (c != null) {
            c.close();
        }
        adapter.notifyDataSetChanged();

        if (promoList.size() == 0) {
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }

    }

    @Override
    public void updatedElements(ArrayList<ElemWindow> arrayList) {
        Log.d("NOTY", "UpdatedElements");
        updatePromoList();
    }

    /**
     @name onResume
     @desc We will resume the SDK session state when the app return from background to foreground
     */
    @Override
    protected void onResume() {
        super.onResume();
        Refs.resumeSession(this);
    }

    /**
     @name onStop
     @desc We will stop the SDK session state when the app go to background from foreground
     */
    @Override
    protected void onStop() {
        super.onStop();
        Refs.stopSession(this);
    }

    /**
     @name onDestroy
     @desc We will take advantage of onDestroy method and its lifecycle within
     the application to close refs session and stop communications between *server and app.
     */
    @Override

    protected void onDestroy() {
        super.onDestroy();
        Refs.endSession();
    }
}
