package com.seeketing.seekdeals.controllers;

/**
 * Created by arivas on 7/3/16.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.DealerItem;
import com.seeketing.seekdeals.models.DealersDatabaseHelper;
import com.seeketing.seekdeals.models.Timer;

/**
 * Created by arivas on 22/1/16.
 */
public class MapFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    GoogleMap myGoogleMap;
    View rootView;
    private HashMap<Marker, Integer> markerDealers = new HashMap<Marker, Integer>();

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapFragment newInstance(int sectionNumber) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public MapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.map_fragment, container, false);
        }

        Sets.trackEventImpr(rootView, "mapa", null);

        initilizeMap();

        return rootView;
    }

    /*@Override
    public void onDestroyView() {
        super.onDestroyView();

        SupportMapFragment f = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }*/

    private void initilizeMap() {
        SupportMapFragment mSupportMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    myGoogleMap = googleMap;
                    myGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(40.3713243, -3.5380695), 6));
                    myGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Sets.trackEventClck(getString(R.string.selTienda) + marker.getTitle());
                            int dealerId = markerDealers.get(marker);
                            Intent i = new Intent(getContext(), DealerProducts.class);
                            i.putExtra("dealerId", dealerId);
                            startActivity(i);
                        }
                    });
                    DealersDatabaseHelper dealersDatabaseHelper = new DealersDatabaseHelper(getContext());
                    List<DealerItem> dealerItemList = dealersDatabaseHelper.getAllItems();

                    if (dealerItemList.size() != 0) {
                        for (int i = 0; i < dealerItemList.size(); i++) {
                            DealerItem dealerItem = dealerItemList.get(i);
                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(dealerItem.getLatitude(), dealerItem.getLongitude())).title(dealerItem.getName()).snippet(dealerItem.getDescription());
                            Marker marker = myGoogleMap.addMarker(markerOption);
                            markerDealers.put(marker, dealerItem.getDealId());
                        }
                    }
                }
            });
        }
    }

}
