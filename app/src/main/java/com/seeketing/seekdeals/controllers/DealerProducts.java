package com.seeketing.seekdeals.controllers;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.seeketing.sdks.sets.Sets;
import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.controllers.DetailActivity;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.CategoriesDatabaseHelper;
import com.seeketing.seekdeals.models.CategoryItem;
import com.seeketing.seekdeals.models.DealItem;
import com.seeketing.seekdeals.models.DealsDatabaseHelper;
import com.seeketing.seekdeals.models.Timer;
import com.seeketing.seekdeals.views.DealItemsListAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class DealerProducts extends AppCompatActivity {

    private ListView listView;
    private DealItemsListAdapter adapter;
    private List<List<DealItem>> dealsCategorized;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_products);

        listView = (ListView) this.findViewById(R.id.listView);

        DealsDatabaseHelper dealsDatabaseHelper = new DealsDatabaseHelper(getApplicationContext());
        CategoriesDatabaseHelper categoriesDatabaseHelper = new CategoriesDatabaseHelper(getApplicationContext());
        List<CategoryItem> categories = categoriesDatabaseHelper.getAllItems();

        if (getIntent().getExtras() != null) {
            dealsCategorized = dealsDatabaseHelper.getDealsForCategoryByDealer(getIntent().getExtras().getInt("dealerId"));
        } else {
            dealsCategorized = dealsDatabaseHelper.getDealsForCategory();
        }

        AppIdData dataUser = new AppIdData(getApplicationContext());
        changeAppParameters(dataUser);

        adapter = new DealItemsListAdapter(getApplicationContext(), R.layout.listview_item, dealsCategorized, categories);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject json = adapter.getObjectByPosition(position);

                int section = 0;
                int row = 0;

                try {
                    section = json.getInt("section");
                    row = json.getInt("row");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (row != 0) {
                    row -= 1;

                    DealItem dealItem = dealsCategorized.get(section).get(row);
                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                    intent.putExtra("id", dealItem.getId());
                    startActivity(intent);
                }
            }
        });

        ViewGroup view = (ViewGroup)getWindow().getDecorView();
        Sets.trackEventNavi("DealerProducts", 50, null);
        Sets.trackEventImpr(view, "DealerProducts", null);
    }

    private void changeAppParameters(AppIdData dataUser) {
        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(dataUser.getName());

        int backgroundColor = Color.parseColor(dataUser.getBackgroundColor());
        int textColor = Color.parseColor(dataUser.getTextColor());
        int color = Color.parseColor(dataUser.getTextColor());
        int textColorOther = Color.argb(100, Color.red(color), Color.green(color), Color.blue(color));

        title.setTextColor(textColor);
        title.setBackgroundColor(backgroundColor);

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.setBackgroundColor(backgroundColor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(backgroundColor);
    }
}
