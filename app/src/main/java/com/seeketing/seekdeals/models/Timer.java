package com.seeketing.seekdeals.models;

import android.view.View;

/**
 * Created by Ignacio Martin on 1/04/16.
 */
public class Timer {

    private static long inicio, fin;
    private static boolean iniciado;
    private static View vistaPtr;

    public static void iniciarCuenta(View vista) {
        inicio = System.currentTimeMillis();
        iniciado = true;
        vistaPtr = vista;
    }

    public static long pararCuenta() {
        fin = System.currentTimeMillis();
        iniciado = false;
        vistaPtr = null;
        return (fin - inicio)/1000;
    }

    public static boolean isIniciado() {
        return iniciado;
    }

    public static View getVista() {
        return vistaPtr;
    }
}
