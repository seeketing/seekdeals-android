package com.seeketing.seekdeals.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 17/3/16.
 */
public class DealersDatabaseHelper extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "model_dealers";
    // Table name
    private static final String DEALERS_ITEMS = "dealers_items";
    // Column names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_LATITUDE = "latitude";
    private static final String COLUMN_LONGITUDE = "longitude";
    private static final String COLUMN_DEALER_ID = "dealerId";

    public DealersDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DEALERS_ITEMS);

        db.execSQL("CREATE TABLE " + DEALERS_ITEMS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_DESCRIPTION + " TEXT NOT NULL,"
                + COLUMN_LATITUDE + " DOUBLE NOT NULL,"
                + COLUMN_LONGITUDE + " DOUBLE NOT NULL,"
                + COLUMN_DEALER_ID + " INTEGER NOT NULL"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void deleteAllItems() {
        SQLiteDatabase db = getReadableDatabase();

        db.delete(DEALERS_ITEMS, null, null);
    }
    /**
     * retrieve all items from the database
     */
    public List<DealerItem> getAllItems() {
        // initialize the list
        List<DealerItem> items = new ArrayList<DealerItem>();

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        // send query
        Cursor cursor = db.query(DEALERS_ITEMS, new String[] {
                COLUMN_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_LATITUDE,
                COLUMN_LONGITUDE,
                COLUMN_DEALER_ID
        }, null, null, null, null, null, null); // get all rows

        if (cursor != null) {
            // add items to the list
            for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                items.add(new DealerItem(cursor.getString(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getInt(4)));
            }

            // close the cursor
            cursor.close();
        }

        // close the database connection
        db.close();

        // return the list
        return items;
    }

    /**
     * Add items to the list
     */
    public void addItems(List<DealerItem> items) {
        if(items != null && items.size() > 0) {
            // obtain a readable database
            SQLiteDatabase db = getWritableDatabase();

            for(DealerItem item : items) {
                addItem(db, item);
            }

            // close the database connection
            db.close();
        }
    }

    /**
     * Add a new item
     */
    private void addItem(SQLiteDatabase db, DealerItem item) {
        // prepare values
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, item.getName());
        values.put(COLUMN_DESCRIPTION, item.getDescription());
        values.put(COLUMN_LATITUDE, item.getLatitude());
        values.put(COLUMN_LONGITUDE, item.getLongitude());
        values.put(COLUMN_DEALER_ID, item.getDealId());

        // add the row
        db.insert(DEALERS_ITEMS, null, values);
    }
}
