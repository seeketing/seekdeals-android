package com.seeketing.seekdeals.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by arivas on 15/3/16.
 */
public class ServerResponse {

    ServerResponseData response;
    Boolean isError;
    String errorDesc;
    String errorNum;
    String errorType;

    public ServerResponse(JSONObject jsonObject) {
        try {
            response = new ServerResponseData(jsonObject.getJSONObject("response"));
            isError = jsonObject.getBoolean("isError");
            errorDesc = jsonObject.getString("errorDesc");
            errorNum = jsonObject.getString("errorNum");
            errorType = jsonObject.getString("errorType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class ServerResponseData {

        Object data;
        Integer records;
        String status;

        public ServerResponseData(JSONObject jsonObject) {
            try {
                data = jsonObject.get("data");
                records = jsonObject.getInt("records");
                status = jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static ServerResponseData parseFromData(String response) {
        try {
            ServerResponse serverResponse = new ServerResponse(new JSONObject(response));

            if (serverResponse.isError) {
                Log.d("parseFromData()", "Reponse error " + serverResponse.errorNum + " type " + serverResponse.errorType + ", " + serverResponse.errorDesc);

                return null;
            }

            return serverResponse.response;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}