package com.seeketing.seekdeals.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 17/3/16.
 */
public class CategoriesDatabaseHelper extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "model_categories";
    // Table name
    private static final String CATEGORIES_ITEMS = "category_items";
    // Column names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_IMAGEURL = "imageUrl";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_COLOR = "color";
    private static final String COLUMN_CAT_ID = "catId";

    public CategoriesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORIES_ITEMS);

        db.execSQL("CREATE TABLE " + CATEGORIES_ITEMS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_IMAGEURL + " TEXT NOT NULL,"
                + COLUMN_IMAGE + " BLOB NOT NULL,"
                + COLUMN_COLOR + " INTEGER NOT NULL,"
                + COLUMN_CAT_ID + " INTEGER NOT NULL"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void deleteAllItems() {
        SQLiteDatabase db = getReadableDatabase();

        db.delete(CATEGORIES_ITEMS, null, null);
    }
    /**
     * retrieve all items from the database
     */
    public List<CategoryItem> getAllItems() {
        // initialize the list
        List<CategoryItem> items = new ArrayList<CategoryItem>();

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        // send query
        Cursor cursor = db.query(CATEGORIES_ITEMS, new String[] {
                COLUMN_NAME,
                COLUMN_IMAGEURL,
                COLUMN_IMAGE,
                COLUMN_COLOR,
                COLUMN_CAT_ID
        }, null, null, null, null, null, null); // get all rows

        if (cursor != null) {
            // add items to the list
            for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                byte[] byteArray = cursor.getBlob(2);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPurgeable = true;
                Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                items.add(new CategoryItem(cursor.getString(0), cursor.getString(1), bm, cursor.getString(3), cursor.getInt(4)));
            }

            // close the cursor
            cursor.close();
        }

        // close the database connection
        db.close();

        // return the list
        return items;
    }

    /**
     * Add items to the list
     */
    public void addItems(List<CategoryItem> items) {
        if(items != null && items.size() > 0) {
            // obtain a readable database
            SQLiteDatabase db = getWritableDatabase();

            for(CategoryItem item : items) {
                addItem(db, item);
            }

            // close the database connection
            db.close();
        }
    }

    /**
     * Add a new item
     */
    private void addItem(SQLiteDatabase db, CategoryItem item) {
        // prepare values
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, item.getName());
        values.put(COLUMN_IMAGEURL, item.getImageUrl());

        Bitmap yourBitmap = item.getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        yourBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        values.put(COLUMN_IMAGE, bos.toByteArray());

        values.put(COLUMN_COLOR, item.getColor());
        values.put(COLUMN_CAT_ID, item.getId());

        // add the row
        db.insert(CATEGORIES_ITEMS, null, values);
    }
}
