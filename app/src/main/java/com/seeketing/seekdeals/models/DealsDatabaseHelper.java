package com.seeketing.seekdeals.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 3/3/16.
 */

public class DealsDatabaseHelper extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "model_items";
    // Table name
    private static final String DEALS_ITEMS = "deal_items";
    // Column names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "title";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_IMAGEURL = "imageUrl";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_ID_DEAL = "idDeal";
    private static final String COLUMN_PRICE = "price";
    private static final String COLUMN_DEALER_ID = "dealer_id";

    private Context mContext;

    public DealsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DEALS_ITEMS);

        db.execSQL("CREATE TABLE " + DEALS_ITEMS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_DESCRIPTION + " TEXT NOT NULL,"
                + COLUMN_IMAGEURL + " TEXT NOT NULL,"
                + COLUMN_IMAGE + " BLOB NOT NULL,"
                + COLUMN_CATEGORY + " INTEGER NOT NULL,"
                + COLUMN_PRICE + " DOUBLE NOT NULL,"
                + COLUMN_ID_DEAL + " INTEGER NOT NULL,"
                + COLUMN_DEALER_ID + " INTEGER NOT NULL"
                + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void deleteAllItems() {
        SQLiteDatabase db = getReadableDatabase();

        db.delete(DEALS_ITEMS, null, null);
    }
    /**
     * retrieve all items from the database
     */
    public List<DealItem> getAllItems() {
        // initialize the list
        List<DealItem> items = new ArrayList<DealItem>();

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        // send query
        Cursor cursor = db.query(DEALS_ITEMS, new String[]{
                COLUMN_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_IMAGEURL,
                COLUMN_IMAGE,
                COLUMN_CATEGORY,
                COLUMN_PRICE,
                COLUMN_ID_DEAL,
                COLUMN_DEALER_ID
        }, null, null, null, null, null, null); // get all rows

        if (cursor != null) {
            // add items to the list
            for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                byte[] byteArray = cursor.getBlob(3);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPurgeable = true;
                Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                items.add(new DealItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), bm, cursor.getInt(4), cursor.getDouble(5), cursor.getInt(6), cursor.getInt(7)));
            }

            // close the cursor
            cursor.close();
        }

        // close the database connection
        db.close();

        // return the list
        return items;
    }

    public List<List<DealItem>> getDealsForCategory() {
        CategoriesDatabaseHelper categoriesDatabaseHelper = new CategoriesDatabaseHelper(mContext);
        List<CategoryItem> categories =  categoriesDatabaseHelper.getAllItems();

        List<List<DealItem>> itemsCategorized = new ArrayList<List<DealItem>>();

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        for (int i = 0; i < categories.size(); i++) {
            CategoryItem category = categories.get(i);

            // send query
            Cursor cursor = db.query(DEALS_ITEMS, new String[]{
                    COLUMN_NAME,
                    COLUMN_DESCRIPTION,
                    COLUMN_IMAGEURL,
                    COLUMN_IMAGE,
                    COLUMN_CATEGORY,
                    COLUMN_PRICE,
                    COLUMN_ID_DEAL,
                    COLUMN_DEALER_ID
            }, COLUMN_CATEGORY + " = " + category.getId(), null, null, null, null, null); // get all rows

            if (cursor != null) {
                List<DealItem> items = new ArrayList<DealItem>();
                // add items to the list
                for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                    byte[] byteArray = cursor.getBlob(3);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPurgeable = true;
                    Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);

                    items.add(new DealItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), bm, cursor.getInt(4), cursor.getDouble(5), cursor.getInt(6), cursor.getInt(7)));
                }

                itemsCategorized.add(items);

                // close the cursor
                cursor.close();
            }
        }

        // close the database connection
        db.close();

        // return the list
        return itemsCategorized;
    }

    public List<List<DealItem>> getDealsForCategoryByDealer(int dealerId) {
        CategoriesDatabaseHelper categoriesDatabaseHelper = new CategoriesDatabaseHelper(mContext);
        List<CategoryItem> categories =  categoriesDatabaseHelper.getAllItems();

        List<List<DealItem>> itemsCategorized = new ArrayList<List<DealItem>>();

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        for (int i = 0; i < categories.size(); i++) {
            CategoryItem category = categories.get(i);

            // send query
            Cursor cursor = db.query(DEALS_ITEMS, new String[]{
                    COLUMN_NAME,
                    COLUMN_DESCRIPTION,
                    COLUMN_IMAGEURL,
                    COLUMN_IMAGE,
                    COLUMN_CATEGORY,
                    COLUMN_PRICE,
                    COLUMN_ID_DEAL,
                    COLUMN_DEALER_ID
            }, COLUMN_CATEGORY + " = " + category.getId() + " AND " + COLUMN_DEALER_ID + " = "  + dealerId, null, null, null, null, null); // get all rows

            if (cursor != null) {
                List<DealItem> items = new ArrayList<DealItem>();
                // add items to the list
                for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                    byte[] byteArray = cursor.getBlob(3);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPurgeable = true;
                    Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);

                    items.add(new DealItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), bm, cursor.getInt(4), cursor.getDouble(5), cursor.getInt(6), cursor.getInt(7)));
                }

                itemsCategorized.add(items);

                // close the cursor
                cursor.close();
            }
        }

        // close the database connection
        db.close();

        // return the list
        return itemsCategorized;
    }

    public DealItem getDealById(int id) {

        // initialize the item
        DealItem item = null;

        // obtain a readable database
        SQLiteDatabase db = getReadableDatabase();

        // send query
        Cursor cursor = db.query(DEALS_ITEMS, new String[]{
                COLUMN_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_IMAGEURL,
                COLUMN_IMAGE,
                COLUMN_CATEGORY,
                COLUMN_PRICE,
                COLUMN_ID_DEAL,
                COLUMN_DEALER_ID
        }, COLUMN_ID_DEAL + " = " + id, null, null, null, null, null); // get row

        if (cursor != null) {
            // add items to the list
            for(cursor.moveToFirst(); cursor.isAfterLast() == false; cursor.moveToNext()) {
                byte[] byteArray = cursor.getBlob(3);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPurgeable = true;
                Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                item = new DealItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), bm, cursor.getInt(4), cursor.getDouble(5), cursor.getInt(6), cursor.getInt(7));
            }

            // close the cursor
            cursor.close();
        }

        // close the database connection
        db.close();

        // return the list
        return item;
    }

    /**
     * Add items to the list
     */
    public void addItems(List<DealItem> items) {
        if(items != null && items.size() > 0) {
            // obtain a readable database
            SQLiteDatabase db = getWritableDatabase();

            for(DealItem item : items) {
                addItem(db, item);
            }

            // close the database connection
            db.close();
        }
    }

    /**
     * Add a new item
     */
    private void addItem(SQLiteDatabase db, DealItem item) {
        // prepare values
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, item.getName());
        values.put(COLUMN_DESCRIPTION, item.getDescription());
        values.put(COLUMN_IMAGEURL, item.getImageUrl());

        Bitmap yourBitmap = item.getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        yourBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        values.put(COLUMN_IMAGE, bos.toByteArray());

        values.put(COLUMN_CATEGORY, item.getCategoryId());
        values.put(COLUMN_PRICE, item.getPrice());
        values.put(COLUMN_ID_DEAL, item.getId());
        values.put(COLUMN_DEALER_ID, item.getDealerId());

        // add the row
        db.insert(DEALS_ITEMS, null, values);
    }
}