package com.seeketing.seekdeals.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 3/3/16.
 */
public class DealItem {

    private Integer id, categoryId, dealerId;
    private String name, description, imageUrl;
    private Double price;
    private Bitmap image;

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public static final String BASE_IMAGE_URL = "http://www.siketing.com/deals/public/media/images/";
    private static final String GET_DEALS = "http://siketing.com/deals/rest?method=getDealsData&params={\"app_id\":APPID}";

    public DealItem(JSONObject jsonObject) {
        try {
            this.description = jsonObject.getString("description");
            this.name = jsonObject.getString("name");
            this.imageUrl = jsonObject.getString("image");

            URL url = new URL(DealItem.BASE_IMAGE_URL + imageUrl);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            this.image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            this.categoryId = jsonObject.getInt("category_id");
            this.price = jsonObject.getDouble("deal_price");
            this.id = jsonObject.getInt("id");
            this.dealerId = jsonObject.getInt("dealer_id");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DealItem(String name, String description, String imageUrl, Bitmap image, Integer categoryId, Double price, Integer id, Integer dealerId) {
        this.description = description;
        this.name = name;
        this.imageUrl = imageUrl;

        this.image = image;
        this.categoryId = categoryId;
        this.price = price;
        this.id = id;
        this.dealerId = dealerId;
    }

    public static boolean getDealsFromServer(Long appId, Context context) {
        String newString = GET_DEALS.replace("APPID", appId.toString());

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(newString);

            urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                ServerResponse.ServerResponseData responseData = ServerResponse.parseFromData(sb.toString());

                if (responseData != null && responseData.records != 0) {
                    JSONArray jsonArray = (JSONArray) responseData.data;

                    List<DealItem> dealItemList = new ArrayList<DealItem>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            dealItemList.add(new DealItem(jsonArray.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    DealsDatabaseHelper dealsDatabaseHelper = new DealsDatabaseHelper(context);
                    dealsDatabaseHelper.deleteAllItems();
                    dealsDatabaseHelper.addItems(dealItemList);

                    return true;
                }
            } else {
                Log.e("getDealsFromServer()", "Response error");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return false;
    }

}
