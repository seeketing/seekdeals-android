package com.seeketing.seekdeals.models;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 17/3/16.
 */
public class DealerItem {
    private String name, description;
    private Double latitude, longitude;
    private int dealId;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int getDealId() {
        return dealId;
    }

    private static final String GET_DEALERS = "http://www.siketing.com/deals/rest/?method=getDealersData&params={\"app_id\":APPID}";

    public DealerItem(JSONObject jsonObject) {
        try {
            this.name = jsonObject.getString("name");
            this.description = jsonObject.getString("description");
            this.latitude = jsonObject.getDouble("latitude");
            this.longitude = jsonObject.getDouble("longitude");
            this.dealId = jsonObject.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public DealerItem(String name, String description, Double latitude, Double longitude, int dealId) {
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dealId = dealId;
    }

    public static boolean getDealersFromServer(Long appId, Context context) {
        String newString = GET_DEALERS.replace("APPID", appId.toString());

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(newString);

            urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                ServerResponse.ServerResponseData responseData = ServerResponse.parseFromData(sb.toString());

                if (responseData.records != 0) {
                    JSONArray jsonArray = (JSONArray) responseData.data;

                    List<DealerItem> dealerItemList = new ArrayList<DealerItem>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            dealerItemList.add(new DealerItem(jsonArray.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    DealersDatabaseHelper dealersDatabaseHelper = new DealersDatabaseHelper(context);
                    dealersDatabaseHelper.deleteAllItems();
                    dealersDatabaseHelper.addItems(dealerItemList);

                    return true;
                }
            } else {
                Log.e("getDealersFromServer()", "Response error");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return false;
    }
}
