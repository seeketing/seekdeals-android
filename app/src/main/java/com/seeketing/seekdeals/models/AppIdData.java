package com.seeketing.seekdeals.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by arivas on 15/3/16.
 */
public class AppIdData {

    private final SharedPreferences sharedPrefs;

    private static final String DATAUSER_URL = "http://siketing.com/deals/rest/?method=getClientDataFromActivationCode&params={\"activation_code\":\"DEMO\"}";
    private static final String IMAGES_PREV_BASE_URL = "http://www.siketing.com/deals/public/media/images/";

    private static final String APP_ID_PREF = "app_id";
    private static final String NAME_PREF = "name";
    private static final String LOGO_PREF = "logo";
    private static final String COLOR_TEXT_PREF = "text_color";
    private static final String COLOR_BACKGROUND_PREF = "bkg_color";
    private static final String CONFIGURED_PREF = "configured";

    private Long appId;
    private String name;
    private String backgroundColor;
    private String textColor;
    private String logo;
    private Boolean configured;

    public Boolean getConfigured() {
        return configured;
    }

    public Long getAppId() {
        return appId;
    }

    public String getName() {
        return name;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public String getLogo() {
        return logo;
    }

    public AppIdData(Context context) {
        sharedPrefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);

        appId = sharedPrefs.getLong(APP_ID_PREF, 0);
        name = sharedPrefs.getString(NAME_PREF, "");

        logo = sharedPrefs.getString(LOGO_PREF, "");

        backgroundColor = sharedPrefs.getString(COLOR_BACKGROUND_PREF, "");
        textColor = sharedPrefs.getString(COLOR_TEXT_PREF, "");

        configured = sharedPrefs.getBoolean(CONFIGURED_PREF, false);
    }

    private void storePersistentData(JSONObject jsonObject) {
        try {
            appId = jsonObject.getLong(APP_ID_PREF);
            sharedPrefs.edit().putLong(APP_ID_PREF, appId).apply();

            name = jsonObject.getString(NAME_PREF);
            sharedPrefs.edit().putString(NAME_PREF, name).apply();

            URL url = new URL(IMAGES_PREV_BASE_URL);
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
            if (image != null)
            {
                image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOS);
                logo = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
                sharedPrefs.edit().putString(LOGO_PREF, logo).apply();
            }

            String bgcolor = jsonObject.getString(COLOR_BACKGROUND_PREF);
            if (!bgcolor.startsWith("#")) {
                bgcolor = "#" + bgcolor;
            }
            backgroundColor = bgcolor;
            sharedPrefs.edit().putString(COLOR_BACKGROUND_PREF, backgroundColor).apply();
            String color = jsonObject.getString(COLOR_TEXT_PREF);
            if (!color.startsWith("#")) {
                color = "#" + color;
            }
            textColor = color;
            sharedPrefs.edit().putString(COLOR_TEXT_PREF, textColor).apply();

            configured = true;
            sharedPrefs.edit().putBoolean(CONFIGURED_PREF, configured).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getUserData(String code) {
        String newString = DATAUSER_URL.replace("DEMO", code);

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(newString);

            urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                ServerResponse.ServerResponseData responseData = ServerResponse.parseFromData(sb.toString());

                if (responseData.records != 0) {
                    try {
                        JSONArray data = (JSONArray) responseData.data;
                        storePersistentData(data.getJSONObject(0));

                        return true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Log.e("getUserData()", "Response error");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return false;
    }
}