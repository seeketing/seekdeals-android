package com.seeketing.seekdeals.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 17/3/16.
 */
public class CategoryItem {
    private Integer id;
    private String name, imageUrl, color;
    private Bitmap image;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getColor() {
        return color;
    }

    public Bitmap getImage() {
        return image;
    }

    private static final String GET_CATEGORIES = "http://www.siketing.com/deals/rest/?method=getDealsCategoriesData&params={\"app_id\":APPID}";

    public CategoryItem(JSONObject jsonObject) {
        try {
            this.name = jsonObject.getString("name");
            this.imageUrl = jsonObject.getString("image");

            URL url = new URL(DealItem.BASE_IMAGE_URL + imageUrl);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            this.image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            this.color = jsonObject.getString("color");
            this.id = jsonObject.getInt("id");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CategoryItem(String name, String imageUrl, Bitmap image, String color, Integer id) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.image = image;
        this.id = id;
        this.color = color;
    }

    public static boolean getCategoriesFromServer(Long appId, Context context) {
        String newString = GET_CATEGORIES.replace("APPID", appId.toString());

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(newString);

            urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                ServerResponse.ServerResponseData responseData = ServerResponse.parseFromData(sb.toString());

                if (responseData.records != 0) {
                    JSONArray jsonArray = (JSONArray) responseData.data;

                    List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            categoryItemList.add(new CategoryItem(jsonArray.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    CategoriesDatabaseHelper categoriesDatabaseHelper = new CategoriesDatabaseHelper(context);
                    categoriesDatabaseHelper.deleteAllItems();
                    categoriesDatabaseHelper.addItems(categoryItemList);

                    return true;
                }
            } else {
                Log.e("getCategoriesFromServer()", "Response error");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return false;
    }
}
