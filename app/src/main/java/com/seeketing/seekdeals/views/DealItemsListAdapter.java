package com.seeketing.seekdeals.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.seeketing.seekdeals.R;
import com.seeketing.seekdeals.models.AppIdData;
import com.seeketing.seekdeals.models.CategoryItem;
import com.seeketing.seekdeals.models.DealItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arivas on 3/3/16.
 */

public class DealItemsListAdapter extends ArrayAdapter<List<DealItem>> {
    private static final int HEADER_ROW = 0;

    private final Context context;
    private List<List<DealItem>> items;
    private List<CategoryItem> categories;
    private List<JSONObject> positions;

    public DealItemsListAdapter(Context context, int layout, List<List<DealItem>> items, List<CategoryItem> categories) {
        super(context, layout, items);
        this.context = context;
        this.items = items;
        this.categories = categories;
        this.positions = new ArrayList<JSONObject>();

        for (int i = 0; i < items.size(); i++) {
            JSONObject json = new JSONObject();
            try {
                json.put("section", i);
                json.put("row", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            positions.add(json);

            for (int j = 0; j < items.get(i).size(); j++) {
                JSONObject json2 = new JSONObject();
                try {
                    json2.put("section", i);
                    json2.put("row", j + 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                positions.add(json2);
            }
        }
    }

    public JSONObject getObjectByPosition(int position) {
        return positions.get(position);
    }

    @Override
    public int getCount() {
        int counter = 0;
        for (int i = 0; i < items.size(); i++) {
            counter += items.get(i).size() + 1;
        }
        return counter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;

        JSONObject json = positions.get(position);

        AppIdData dataUser = new AppIdData(getContext());

        try {
            int row = json.getInt("row");
            int section = json.getInt("section");

            switch (row) {
                case HEADER_ROW:
                    rowView = inflater.inflate(R.layout.separatorlist_item, parent, false);

                    TextView sectionTitle = (TextView) rowView.findViewById(R.id.textSeparator);
                    sectionTitle.setText(categories.get(section).getName());

                    int backgroundColor = Color.parseColor("#" + categories.get(section).getColor());
                    sectionTitle.setBackgroundColor(backgroundColor);

                    break;
                default:
                    rowView = inflater.inflate(R.layout.listview_item, parent, false);

                    TextView textViewNam = (TextView) rowView.findViewById(R.id.name);
                    TextView textViewDes = (TextView) rowView.findViewById(R.id.description);
                    ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);

                    int textColor = Color.parseColor(dataUser.getTextColor());
                    int color = Color.parseColor(dataUser.getTextColor());
                    int textColorOther = Color.argb(100, Color.red(color), Color.green(color), Color.blue(color));

                    textViewNam.setText(items.get(section).get(row - 1).getName());
                    textViewNam.setTextColor(textColor);

                    textViewDes.setText(items.get(section).get(row - 1).getDescription());
                    textViewDes.setTextColor(textColorOther);
                    imageView.setImageBitmap(items.get(section).get(row - 1).getImage());

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rowView;
    }

    public void upDateEntries(List<List<DealItem>> entries) {
        items = entries;
        notifyDataSetChanged();
    }
}