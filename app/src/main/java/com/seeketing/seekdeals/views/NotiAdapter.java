package com.seeketing.seekdeals.views;

/**
 * Created by arivas on 9/2/16.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.seeketing.sdks.refs.ElemWindow;
import com.seeketing.sdks.refs.util.Utilities;

import java.util.ArrayList;

public class NotiAdapter extends BaseAdapter {

    private ArrayList<ElemWindow> list_objects;
    private LayoutInflater mInflater;
    private int userView;
    private String packageName;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    private class ViewHolder {
        public TextView titleViewHolder;
        public TextView descViewHolder;
    }

    public NotiAdapter(Context context, int layoutResId,
                       ArrayList<ElemWindow> objects) {
        super();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userView = layoutResId;
        list_objects = objects;
    }

    @Override
    public int getCount() {
        return list_objects.size();
    }

    @Override
    public ElemWindow getItem(int position) {
        return list_objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getNotificationId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View targetView = convertView;

        final ViewHolder tempView;

        if (targetView == null) {
            targetView = mInflater.inflate(userView, parent, false);

            tempView = new ViewHolder();
            if(packageName != null){
                tempView.titleViewHolder = (TextView) targetView.findViewById(Utilities.getResourceIdByName(packageName, "id", "promo_title"));
                tempView.descViewHolder = (TextView) targetView.findViewById(Utilities.getResourceIdByName(packageName, "id", "promo_desc"));
            }

            targetView.setTag(tempView);
        } else {
            tempView = (ViewHolder) targetView.getTag();
        }

        if(packageName != null){
            ElemWindow elem = getItem(position);
            tempView.titleViewHolder.setText(elem.getTitle());
            tempView.descViewHolder.setText(elem.getDescription());
        }

        if (position % 2 == 1) {
            targetView.setBackgroundColor(Color.GRAY);
        } else {
            targetView.setBackgroundColor(Color.BLACK);
        }

        return targetView;
    }

}
